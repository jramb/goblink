# Go Blink!

Suppose you have a Blink(1) unit. 
Switching it on and off locally is not entirely as satisfactory as advertised
after a while...

With this software you can easily connect your blink(1) unit to a MQTT and thus
enable it as a IOT entity and control it as such. The node running the blink(1) unit
does not need to be anywhere near the controlling instance, but can be anywhere in the world
where it can access the same MQTT. You can for example have your blink(1)
on your notebook at work and still control it from your homes Home Assistant,
for example when someone rings the doorbell, when someone comes home, at sunset,
etc.

Currently made primarily for Home Assistant it should be able to cooperate with any MQTT
center, including but not limited your own tools and units.

## Install
Download and install using `go get gitlab.com/jramb/goblink`.

If you get `fatal error: usb.h: No such file or directory` then
try installing `sudo apt-get install libusb-dev`
or `sudo pacman -S libusb-compat`.

You might need to copy the rules file to udev rules also:
`sudo cp 51-blink1.rules /etc/udev/rules.d/`


## Setup

Configure your local `goblink.toml`,
to something like the following. `goblink.toml`
can be in any of several locations, for example
in the current working directory:

```toml
[blink]
# led=2 # both = 0 , sides = 1 or 2
topic-prefix="goblink/blink1/light1"
set-topic="set"
state-topic="state"

[mqtt]
broker="tcp://iot.eclipse.org:1883"
# broker="tls://iot.eclipse.org:8883"
# client-id= <hostname>
username="jack"
password="jacksPW"
subscribe=["goblink/#","other/#"]
```

Start `goblink` in the same location where the `goblink.toml` file
is.

Configure your home assistant to use **MQTT**
and your endpoint:

```yaml
light:
  - platform: mqtt_json
    name: Blink1
    command_topic: "goblink/blink1/light1/set"
    state_topic: "goblink/blink1/light1/state"
    brightness: true
    color_temp: true
    retain: false
    optimistic: false
    rgb: true
```


Restart your home assistant.

Enjoy your new light!



