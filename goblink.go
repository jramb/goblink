package main

import (
	"fmt"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	blink1 "github.com/g3force/go-blink1"
	//blink1 "github.com/hink/go-blink1"
	"github.com/jramb/chalk"
	"github.com/spf13/viper"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"log"
	"math"
	"os"
	"os/exec"
	"strconv"
)

var mqttClient mqtt.Client
var dev *blink1.Device
var devLED uint8
var setTopic string
var stateTopic string

var cheerlightsClient mqtt.Client

type BlinkType struct {
	Led        uint8  `mapstructure:"led"`
	SetTopic   string `mapstructure:"set-topic"`
	StateTopic string `mapstructure:"state-topic"`
}
type MqttType struct {
	Broker    string   `mapstructure:"broker"`
	Username  string   `mapstructure:"username"`
	Password  string   `mapstructure:"password"`
	ClientID  string   `mapstructure:"client-id"`
	Subscribe []string `mapstructure:"subscribe"`
}
type CheerType struct {
	Broker   string `mapstructure:"broker"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
	ClientID string `mapstructure:"client-id"`
	Auto     bool   `mapstructure:"auto"`
	APIKey   string `mapstructure:"api-key"`
	Topic    string `mapstructure:"topic"`
}
type ConfigType struct {
	Debug bool        `mapstructure:"debug"`
	Blink []BlinkType `mapstructure:"blink"`
	Mq    []MqttType  `mapstructure:"mqtt"`
	Cheer CheerType   `mapstructure:"cheerlights"`
}

var config *ConfigType

func clamp(v float64, low float64, high float64) uint8 {
	if v > high {
		v = high
	}
	if v < low {
		v = low
	}
	return uint8(v)
}

// inval comes 153..500
// http://www.tannerhelland.com/4435/convert-temperature-rgb-algorithm-code/
func colorTemperatureToRGB(mirek float64) (r uint8, g uint8, b uint8) {
	// 153 mirek = 6500 K, 500 mirek = 2000 K
	//temp is in kelvin / 100 (kelvin is 1000 to 40000)
	min := 2000.
	max := 6500.
	temp := (max + (mirek-153)/(500-153)*(min-max)) / 100

	fmt.Printf("mirek = %.2f, temp= %.2f\n", mirek, temp)

	var red, green, blue float64
	if temp <= 66 {
		red = 255
		green = temp
		green = 99.4708025861*math.Log(green) - 161.1195681661

		if temp <= 19 {
			blue = 0
		} else {
			blue := temp - 10
			blue = 138.5177312231*math.Log(blue) - 305.0447927307
		}
	} else {
		red := temp - 60
		red = 329.698727446 * math.Pow(red, -0.1332047592)

		green = temp - 60
		green = 288.1221695283 * math.Pow(green, -0.0755148492)
		blue = 255
	}
	r = clamp(red, 0, 255)
	g = clamp(green, 0, 255)
	b = clamp(blue, 0, 255)
	return
}

func makeState(r uint8, g uint8, b uint8, fade int, dur int) blink1.State {
	return blink1.State{
		Red:      r,
		Green:    g,
		Blue:     b,
		LED:      blink1.LEDAll,
		FadeTime: time.Duration(fade) * time.Millisecond,
		Duration: time.Duration(dur) * time.Millisecond}
}

func applyBrightness(state *blink1.State, brightness float64) {
	state.Red = uint8(float64(state.Red) * brightness / 255)
	state.Green = uint8(float64(state.Green) * brightness / 255)
	state.Blue = uint8(float64(state.Blue) * brightness / 255)
	if state.Red > 255 {
		state.Red = 255
	}
	if state.Green > 255 {
		state.Green = 255
	}
	if state.Blue > 255 {
		state.Blue = 255
	}
}

func maxInt(a uint8, b uint8) uint8 {
	if a > b {
		return a
	} else {
		return b
	}
}

func getBrightness(st blink1.State) uint8 {
	return maxInt(maxInt(st.Red, st.Green), st.Blue)
}

func maximizeColor(st *blink1.State) {
	maxVal := float64(getBrightness(*st))
	st.Red = uint8(float64(st.Red) * 255 / maxVal)
	st.Green = uint8(float64(st.Green) * 255 / maxVal)
	st.Blue = uint8(float64(st.Blue) * 255 / maxVal)
}

var lastOnState *blink1.State
var lastOnBrightness *float64

func colorToJS(colorCode string) string {
	// fmt.Println(colorCode[1:3], colorCode[3:5], colorCode[5:7])
	r, _ := strconv.ParseInt("0x"+colorCode[1:3], 0, 9)
	g, _ := strconv.ParseInt("0x"+colorCode[3:5], 0, 9)
	b, _ := strconv.ParseInt("0x"+colorCode[5:7], 0, 9)
	ret := fmt.Sprintf(`{"state":"ON","color":{"r":%d,"g":%d,"b":%d}}`, r, g, b)
	// fmt.Println("colorToJS:", ret)
	return ret
}

func blinkSetState(stateJS string) {
	if dev != nil {
		root := gjson.Parse(stateJS)
		state := root.Get("state")
		rgb := root.Get("color")
		brightness := root.Get("brightness")
		transion := root.Get("transion")
		var bl blink1.State
		if state.Str == "OFF" {
			bl = blink1.OffState
		} else {
			whiteValue := root.Get("white_value")
			colorTemp := root.Get("color_temp")
			effect := root.Get("effect")

			if rgb.Exists() {
				bl = makeState(
					uint8(rgb.Get("r").Num),
					uint8(rgb.Get("g").Num),
					uint8(rgb.Get("b").Num),
					10, 10)
				lastOnState = &bl
				if brightness.Exists() {
					lastOnBrightness = &brightness.Num
					applyBrightness(&bl, brightness.Num)
				} else if lastOnBrightness != nil {
					applyBrightness(&bl, *lastOnBrightness)
				}
			} else if brightness.Exists() {
				if lastOnState != nil {
					bl = *lastOnState
					maximizeColor(&bl)
				} else {
					bl = makeState(0xff, 0xff, 0xff, 10, 10)
					lastOnState = &bl
				}
				lastOnBrightness = &brightness.Num
				applyBrightness(&bl, brightness.Num)
			} else if whiteValue.Exists() {
				lastOnBrightness = &whiteValue.Num
				bl = makeState(0xff, 0xff, 0xff, 10, 10)
				lastOnState = &bl
				applyBrightness(&bl, *lastOnBrightness)
			} else if colorTemp.Exists() {
				r, g, b := colorTemperatureToRGB(colorTemp.Num)
				bl = makeState(r, g, b, 10, 10)
			} else if effect.Exists() {
				switch effect.Str {
				case "normal":
					cheerlights(false)
					bl = blink1.OffState
				case "cheerlights":
					cheerlights(true)
					bl = blink1.OffState
				case "crazy":
					effectCrazy()
					bl = blink1.OffState
				case "halloween":
					bl = makeState(0xff, 0x99, 0x00, 10, 10)
				}
			} else {
				// just "ON"
				if lastOnState != nil {
					bl = *lastOnState
				} else {
					bl = makeState(0xff, 0xff, 0xff, 10, 10)
				}
			}
		}
		if transion.Exists() {
			bl.FadeTime = time.Duration(transion.Num) * time.Second
			bl.Duration = bl.FadeTime
		}
		bl.LED = devLED
		////
		_ = dev.SetState(bl)
		////
		if rgb.Exists() {
			stateJS, _ = sjson.Set(stateJS, "color.r", bl.Red)
			stateJS, _ = sjson.Set(stateJS, "color.g", bl.Green)
			stateJS, _ = sjson.Set(stateJS, "color.b", bl.Blue)
		}
		// stateJS, _ = sjson.Set(stateJS, "brightness", uint8(0.299*float64(bl.Red)+0.587*float64(bl.Green)+0.114*float64(bl.Blue)))
		stateJS, _ = sjson.Set(stateJS, "brightness", uint8(math.Max(math.Max(float64(bl.Red), float64(bl.Green)), float64(bl.Blue))))
	}

	token := mqttClient.Publish(stateTopic, 0, false, stateJS)
	fmt.Printf(chalk.Cyan.Color("-- Send to '%s':\n   %s\n"),
		stateTopic, stateJS)
	token.Wait()
}

func singleBlink() {
	if dev == nil {
		fmt.Println("Blink device not ready")
		return
	}
	b1 := blink1.State{Red: 0xff, Green: 0x99, Blue: 0x00,
		LED:      blink1.LEDAll,
		FadeTime: time.Duration(100) * time.Millisecond,
		Duration: time.Duration(100) * time.Millisecond}
	b2 := blink1.State{Red: 0xff, Green: 0x00, Blue: 0x00,
		LED:      blink1.LEDAll,
		FadeTime: time.Duration(1000) * time.Millisecond,
		Duration: time.Duration(1000) * time.Millisecond}
	off := blink1.State{Duration: time.Duration(1) * time.Second,
		FadeTime: time.Duration(1) * time.Second}

	flashFade := []blink1.State{b1, b2, off} //, blink1.OffState]
	dev.RunPattern(
		&blink1.Pattern{
			Repeat:      0,
			RepeatDelay: 100 * time.Millisecond,
			States:      flashFade})
	// err := dev.SetState(blink1.State{Blue: 255,
	// 	LED:      blink1.LEDAll,
	// 	FadeTime: time.Duration(100) * time.Millisecond,
	// 	Duration: time.Duration(3) * time.Second})
	// if err != nil {
	// 	panic(err)
	// }
}

func connLostHandler(c mqtt.Client, err error) {
	fmt.Printf(chalk.Red.Color("Connection lost, reason: %v\n"), err)
	//Perform additional action...
	for {
		time.Sleep(10 * time.Second)
		if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
			fmt.Printf(chalk.Red.Color("Reconnect failed, reason: %v\n"), err)
		} else {
			fmt.Printf(chalk.Green.Color("Reconnected!\n"))
			return
		}
	}
}

func messageHandler(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("*** %s: %s\n", chalk.Green.Color(time.Now().Format("2006-01-02 15:04:05")),
		chalk.Magenta.Color(msg.Topic()))
	fmt.Printf("%s\n", chalk.Blue.Color(string(msg.Payload())))
	if msg.Topic() == setTopic {
		blinkSetState(string(msg.Payload()))
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	// if cfgFile != "" { // enable ability to specify config file via flag
	// 	viper.SetConfigFile(cfgFile)
	// }

	viper.SetConfigName("goblink")                                  // name of config file (without extension)
	viper.AddConfigPath(".")                                        //current
	viper.AddConfigPath("$HOME/.config")                            //config directory
	viper.AddConfigPath("$HOME")                                    // adding home directory as first search path
	if userprofile := os.Getenv("USERPROFILE"); userprofile != "" { //Windows
		viper.AddConfigPath(userprofile)
	}
	//viper.AutomaticEnv()         // read in environment variables that match

	// viper.WatchConfig()
	// viper.OnConfigChange(func(e fsnotify.Event) {
	// 	fmt.Println("Config file changed:", e.Name)
	// })

	hostname, _ := os.Hostname()
	viper.SetDefault("mqtt.client-id", hostname)
	viper.SetDefault("blink.led", 0)
	// viper.SetDefault("mqtt.subscribe", map[string]string{"tag": "tags", "category": "categories"})
	viper.SetDefault("cheerlights.broker", "tls://mqtt.thingspeak.com:8883")
	viper.SetDefault("cheerlights.username", "any")
	viper.SetDefault("cheerlights.topic", "channels/1417/subscribe/fields/field2")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file: " + viper.ConfigFileUsed())
	}
	//RootCmd.DebugFlags()

	config = new(ConfigType)
	if err := viper.Unmarshal(config); err != nil {
		panic(fmt.Errorf("error parsing config file, %v", err))
	}
}

func initMQTT() {
	//define a function for the default message handler
	for _, m := range config.Mq {
		broker := m.Broker //viper.GetString("mqtt.broker")
		if broker == "" {
			return
		}

		clientID := m.ClientID //viper.GetString("mqtt.client-id")

		username := m.Username //config.viper.GetString("mqtt.username")
		opts := mqtt.NewClientOptions().
			AddBroker(broker).
			// SetCleanSession(false).
			SetClientID(clientID).
			SetUsername(username).
			SetPassword(m.Password)
		opts.SetDefaultPublishHandler(messageHandler)
		opts.SetConnectionLostHandler(connLostHandler)

		mqttClient = mqtt.NewClient(opts)
		if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
		fmt.Printf("Connected to broker %s using client ID %s.\n", broker, clientID)

		devLED = uint8(config.Blink[0].Led)

		// topicprefix := viper.GetString("blink.topic-prefix") //+ "/" + username
		setTopic = config.Blink[0].SetTopic
		stateTopic = config.Blink[0].StateTopic

		//at a maximum qos of zero, wait for the receipt to confirm the subscription
		for k, sub := range m.Subscribe { //viper.GetStringSlice("mqtt.subscribe") {
			fmt.Printf("Subscribe %2d: %s\n", k, sub)
			if token := mqttClient.Subscribe(sub, 0, nil); token.Wait() && token.Error() != nil {
				fmt.Println(token.Error())
				os.Exit(1)
			}
		}
	}
}

func waitForMessages() {
	// for mqttClient.IsConnected() {
	for {
		time.Sleep(10 * time.Second)
	}
}

func endMQTT() {
	if mqttClient.IsConnected() {
		mqttClient.Disconnect(250)
		fmt.Println("MQTT connection disconnected.")
	}
	if cheerlightsClient.IsConnected() {
		cheerlightsClient.Disconnect(250)
		fmt.Println("Cheerlights connection disconnected.")
	}
}

func cheerlightsHandler(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("*** %s: %s\n", chalk.Green.Color(time.Now().Format("2006-01-02 15:04:05")),
		chalk.Magenta.Color(msg.Topic()))
	fmt.Printf(chalk.Yellow.Color("Cheerlight: %s\n"), chalk.Blue.Color(string(msg.Payload())))
	// if msg.Topic() == setTopic {
	blinkSetState(colorToJS(string(msg.Payload())))
	// 	blinkSetState(string(msg.Payload()))
	// }
}

func cheerlights(active bool) {
	if active && cheerlightsClient == nil {
		opts := mqtt.NewClientOptions().
			AddBroker(config.Cheer.Broker).
			// SetCleanSession(false).
			SetClientID(config.Mq[0].ClientID).
			SetUsername(config.Cheer.Username).
			SetPassword(config.Cheer.APIKey).
			SetDefaultPublishHandler(cheerlightsHandler).
			SetConnectionLostHandler(connLostHandler)
		cheerlightsClient = mqtt.NewClient(opts)
		if token := cheerlightsClient.Connect(); token.Wait() && token.Error() != nil {
			fmt.Printf(chalk.Red.Color("Could not connect: "), token.Error())
			return
		}
		fmt.Printf("Connected to %s\n", config.Cheer.Broker)
		topic := config.Cheer.Topic
		if token := cheerlightsClient.Subscribe(topic, 0, nil); token.Wait() && token.Error() != nil {
			fmt.Println(token.Error())
			cheerlightsClient.Disconnect(10) // after 10 ms
			return
		} else {
			fmt.Printf("Subscribed to %s\n", topic)
		}
	} else { // switch off
		if cheerlightsClient != nil {
			cheerlightsClient.Disconnect(10) // after 10 ms
			fmt.Printf("Disconnected from %s\n", config.Cheer.Broker)
			cheerlightsClient = nil
		}
	}
}

func effectCrazy() {
	fmt.Println("Dev", dev)
	err := dev.SetState(blink1.State{Red: 255,
		LED:      blink1.LEDAll,
		FadeTime: time.Duration(100) * time.Millisecond,
		Duration: time.Duration(3) * time.Second})
	if err != nil {
		panic(err)
	}
	time.Sleep(300 * time.Millisecond)
	for i := 0; i < 300; i++ {
		dev.SetState(blink1.State{Green: 255, Duration: 1 * time.Millisecond})
		dev.SetState(blink1.State{Blue: 255})
		dev.SetState(blink1.State{Red: 255})
	}
	dev.SetState(blink1.OffState)
	time.Sleep(time.Second)
	bl := blink1.State{Blue: 255, Duration: time.Second, FadeTime: time.Second}
	//flashFade := make([]blink1.State, 2)
	flashFade := []blink1.State{bl, blink1.OffState} //, blink1.OffState]
	dev.RunPattern(
		&blink1.Pattern{
			Repeat:      0,
			RepeatDelay: 100 * time.Millisecond,
			States:      flashFade})

}

func main() {
	initConfig()

	if viper.GetBool("debug") {
		fmt.Println("Debug enabled")
		viper.Debug()
	}
	if viper.GetBool("mqtt.debug") {
		mqtt.DEBUG = log.New(os.Stderr, "DEBUG ", log.Ltime)
	}

	var err error
	dev, err = blink1.OpenNextDevice()
	defer dev.Close()

	err = dev.SetState(blink1.OffState)
	if err != nil {
		fmt.Println("Applying blink1-work-around")
		outerr, err := exec.Command("blink1-tool", "--list").CombinedOutput()
		fmt.Printf("%s\n", outerr)
		if err != nil {
			return
			// panic(err)
		}
		err = dev.SetState(blink1.OffState)
		// dev, err = blink1.OpenNextDevice()
		if err != nil {
			// still error? give up!
			panic(err)
		}
	}

	initMQTT()
	defer endMQTT()

	// if viper.GetBool("cheerlights.auto") {
	if config.Cheer.Auto {
		cheerlights(true)
	}
	waitForMessages()
	fmt.Println("Done")
}
